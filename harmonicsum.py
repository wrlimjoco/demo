def harmonic_sum(n):
    if n < 2:
        return 1
    else:
        return 1/n + harmonic_sum(n-1)  # reciprocal of the nth integer + sum of the previous reciprocals

def main():
    input_num = int(input("Input a number: "))
    if input_num > 0:
        print("Harmonic Sum", harmonic_sum(input_num))
    else:
        print("Error! The number you inputed is invalid.")

if __name__ == "__main__":
    main()