from __future__ import print_function

def main():
    instr = input("Please input a list of strings separated by commas: ")

    lst = instr.split(",")

    for item in lst:
        print(item)

if __name__ == "__main__":
    main()