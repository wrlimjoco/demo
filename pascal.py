def pascal(n):
    if n == 1:
        return [1]
    else:
        line = [1]
        prev_line = pascal(n-1)
        for i in range(len(prev_line)-1):
            line.append(prev_line[i] + prev_line[i+1])
        line.append(1)
    return line

def main():
    input_num = int(input("Input a number: "))
    if input_num > 0:
        print("Pascal:", pascal(input_num))
    else:
        print("Error! The number you inputed is invalid.")

if __name__ == "__main__":
    main()