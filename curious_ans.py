from __future__ import print_function
import math

def main():
    truesum = 0
    for num in range(3,1000001):
        ssum, numcopy = 0, num
        while numcopy:
            ssum += math.factorial((numcopy % 10))
            numcopy //= 10
        if ssum == num:
            truesum += ssum
    print(truesum)

if __name__ == "__main__":
    main()
