eee = {111: "Introduction to Programming and Computation",
        113: "Introduction to Electrical and Electronics Engineering Systems",
        118: "Electrical and Electronics Engineering Laboratory 1"
        }

print(eee)
print(type(eee))
print(eee[111])
print(type(eee[111]))
#Bad
#print(eee["Introduction to Programming and Computation"])
print(eee.keys())
print(type(eee.keys()))
a1 = list(eee.keys())
print(a1)
print(type(a1))
eee[121] = "Data Structures and Algorithms in Electrical and Electronics Engineering"
eee[111] = "Programming 1"
print(eee)
del eee[118]
entry = eee.pop(113)
print(entry)
print(type(entry))
print(eee)

#Good practice
if 119 in eee:
    print(eee[119])
else:
    print("Invalid Key")

#Looping through a dictionary
for k, v in eee.items():
    print(k, v)
    print(type(k))
    print(type(v))


