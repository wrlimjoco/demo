"""
    This code snippet checks if the input intger is valid, and if it is, allows you to print the integer
"""
val1 = None
while val1 is None:
    try:
        val1 = int(input("Enter an integer: "))
    except ValueError:
        val1 = None
        print("Invalid input")
print(val1)
