def test_func():
    """This function does trivial printing"""
    print("Test Function is working!")

def nice_func(a = 0, b = 0):
    """This function does some quick maths"""
    s = a + b
    print(a, "+", b, "=", s, "quick maths!")
    print("{} + {} = {} quick maths again!".format(b, a, s))
    # For more formatting tricks, visit: https://pyformat.info/
    return s

# Two ways to get docstring
# Method 1: source_file.fcn_name.__doc__ (Note the lack of parentheses on fcn_name!)
# Method 2: from inspect import *
#           getdoc(source_file.fcn_name) (Note the lack of parentheses on fcn_name!)