from __future__ import print_function

def compare_two(x, y):
    if (x >= y):
        return x
    else:
        return y

def compare_three(a, b, c):
    v = compare_two(a, b)
    w = compare_two(v, c)
    return w

def main():
    a = int(input("Enter number: "))
    b = int(input("Enter number: "))
    c = int(input("Enter number: "))
    x = compare_three(a, b, c)
    print(x)

if __name__ == "__main__":
    main()