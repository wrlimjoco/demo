def mult(x, y):
    if y == 1:
        return x
    return x + mult(x, y-1)

def main():
    x = int(input("Enter x: "))
    y = int(input("Enter y: "))
    print("{} * {} is".format(x,y), mult(x, y))

if __name__ == "__main__":
    main()