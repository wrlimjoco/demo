quote = input("Enter a quote: ")
state = True
for letter in quote:
    if (letter == '"' or letter == '\'') and state == True:
        print("``",end='')
        state = False
    elif (letter == '"' or letter == '\'') and state == False:
        print("\'\'",end='')
        state = True
    else:
        print(letter,end='')
print()
