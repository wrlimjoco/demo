def fib(n):
    if n == 0 or n == 1:
        return n
    return fib(n-1) + fib(n-2)

def main():
    n = int(input("Enter desired term of Fibonacci series: "))
    print("{}th term is: ".format(n), fib(n))

if __name__ == "__main__":
    main()