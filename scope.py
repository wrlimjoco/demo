from __future__ import print_function

def foo(x):
    x = x + 2
    print("x in foo(x) is =", x)
    return x

def main():
    x = 3
    y = foo(x)
    print("x outside fcn/in main is =", x)
    print("y =", y)

if __name__ == "__main__":
    main()