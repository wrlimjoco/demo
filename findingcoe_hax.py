from __future__ import print_function

def main():
    long_string = input("Enter a string: ")
    cnt = long_string.count("coe")
    print("I found 'coe'", cnt, "times")

if __name__ == "__main__":
    main()