def namescore(name):
    #score = 0
    #for char in name.strip('"'):
    #    score += (ord(char) - 64)
    #return score
    return sum((ord(char) - 64) for char in name.strip('"'))

def main():
    with open("p022_names.txt") as fh:
        names = sorted(fh.read().split(','))
    #total = 0
    #for n in range(len(names)):
    #    total += ((n+1) * namescore(names[n]))
    #print(total)
    print(sum(((n+1) * namescore(names[n])) for n in range(len(names))))

if __name__ == "__main__":
    main()