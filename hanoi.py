from __future__ import print_function

def hanoi(n):
    if n == 1:
        return 1
    else:
        return 2 * hanoi(n-1) + 1

def main():
    inp = int(input("Enter integer > 0: "))
    while (inp <= 0):
        inp = int(input("Enter integer > 0: "))
    print("Number of moves is:", hanoi(inp))

if __name__ == "__main__":
    main()
