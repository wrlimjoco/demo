def f(n):
    if n == 0:
        print(n)
        return None
    print(n)
    f(n-1)
    #put additional code here

def main():
    n = int(input("Enter n: "))
    f(n)

if __name__ == "__main__":
    main()
