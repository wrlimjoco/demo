"""This is a basic circle module"""
pi = 3.14159

def circumference(radius=0):
    """This calculates the circumference of a circle given the radius"""
    return 2 * pi * radius

def area(radius=0):
    """This calculates the area of a circle given the radius"""
    return pi * radius * radius

