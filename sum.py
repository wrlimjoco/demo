from __future__ import print_function

#Calculates the summation from 1 to x
def summation(x):
    if (x < 1):
        print("Input is too low")
    else:
        sum = 0
        for i in range(1, x+1):
            #print(sum)
            sum += i
        return sum

def main():
    num = input("Enter a number: ")
    sum = summation(int(num))
    if (sum == None):
        print("Run the program again")
    else:
        print("The summation from 1 to", num, "is", sum)

if __name__ == "__main__":
    main()