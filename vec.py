from math import sqrt

class Vec2d(object):
    """A Basic Custom 2D Physics Vector Object"""
    def __init__(self, x=0, y=0):
        """Create a 2d vector"""
        self.x = x
        self.y = y
    def magnitude(self):
        """Returns the magnitude of the 2d vector"""
        return sqrt( (self.x)**2 + (self.y)**2 )
    def assign(self, x=0, y=0):
        """Methods to reassign the values of the 2d vector"""
        self.x = x
        self.y = y
    def __repr__(self):
        """Returns a printable representation of the 2d vector"""
        return '[' + str(self.x) + ' ' + str(self.y) + ']'
    def __str__(self):
        """Returns a string version of the vector"""
        return '[' + str(self.x) + ' ' + str(self.y) + ']'
    def __float__(self):
        """Returns the magnitude of the vector as a float"""
        return self.magnitude()
    def __iter__(self):
        """Returns a 2d vector converted to a sequence type composed of singletons (e.g. list, tuple, set)"""
        yield self.x
        yield self.y
    #def __tuple__(self):
    #    """Not needed. __iter__() does this"""
    #    return (self.x, self.y)
    #def __list__(self):
    #    return [self.x, self.y]
    def __add__(self, o):
        """Defines addition for 2d vectors"""
        if type(o) == Vec2d:
            return Vec2d(self.x+o.x, self.y+o.y)
        else:
            raise NotImplementedError
    def __sub__(self, o):
        """Defines subtraction for 2d vectors"""
        if type(o) == Vec2d:
            return Vec2d(self.x-o.x, self.y-o.y)
        else:
            raise NotImplementedError
    def __mul__(self, o):
        """Defines scalar multiplication of 2d vectors. Vec2d must be on the right side of the multiplication symbol."""
        if type(o) == int or type(o) == float:
            return Vec2d(self.x*o, self.y*o)
        else:
            raise NotImplementedError
    def __rmul__(self, o):
        """Defines scalar multiplication of 2d vectors. Vec2d must be on the left side of the multiplication symbol."""
        if type(o) == int or type(o) == float:
            return Vec2d(self.x*o, self.y*o)
        else:
            raise NotImplementedError

def main():
    vec1 = Vec2d()
    vec2 = Vec2d(3, 5)
    print(vec1.magnitude())
    print(vec2.magnitude())
    vec1.assign(3,4)
    vec2.__init__(3,6)
    print(vec1.magnitude())
    print(vec1)
    print(vec2)
    print(str(vec1))
    print(str(vec2))
    print(float(vec1))
    print(float(vec2))
    lst1 = list(vec1)
    tup1 = tuple(vec1)
    print(lst1)
    print(tup1)
    print(list(vec1))
    print(tuple(vec1))
    vec1.assign(1,4)
    print(vec1)
    print(vec2)
    vec_a = vec1 + vec2
    print(vec_a)
    vec3 = 2*vec1 + 3*vec2
    vec4 = 3*vec1 - 2*vec2
    vec5 = vec1*4 + vec2*7
    print(vec3)
    print(vec4)
    print(vec5)

    try:
        vecx = vec1 + 2
    except NotImplementedError:
        print("Addition of Vec2d and scalar not implemented")
if __name__ == "__main__":
    main()
