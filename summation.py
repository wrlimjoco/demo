def summation(n):
    if n == 0 or n == 1:
        return n
    return n + summation(n-1)

def main():
    n = int(input("Enter number: "))
    print("Summation from 1 to n (k=1) is:", summation(n))

if __name__ == "__main__":
    main()