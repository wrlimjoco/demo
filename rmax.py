def get_max_from_list(num_list):
    if len(num_list) == 1:
        return (num_list[0])
    else:
        A = num_list[0]
        B = get_max_from_list(num_list[1:len(num_list)])

        if (A > B):
            return A
        else:
            return B

def main():
    numbers = [2444,1,23,4,7,1,231,231,2,532,1008,1,100]
    print(get_max_from_list(numbers))

if __name__ == "__main__":
    main()