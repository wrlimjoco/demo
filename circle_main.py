import circle

def main():
    #help(circle)
    print(circle.pi)
    print(circle.area(3))
    print(circle.circumference(3))
    print(circle.circumference().__doc__)

if __name__ == "__main__":
    main()
