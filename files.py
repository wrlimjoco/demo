#import os
#open/create a file named 'test'
fh = open('ftest', 'wb')
#write stuff to file
fh.write('I am a pretty string'.encode('utf-8'))
#close file
fh.close()

#What's the difference between a text file and a binary file?
#bytes() #immutable type
#bytearray() #mutable type
#.encode('ascii') -> encoding
#bytes(data, encoding='ascii')

#with open('filename', 'mode') as f:
#    #do_stuff

#file_length_in_bytes = os.path.getsize("ftest.txt")
#f.seek(offset, starting_point)
#  offset is number of bytes from starting_point
#  starting_point is 0 by default, which is beginning of file
#  starting_point 1 is start from current position in file
#  starting_point 2 is end of file, requires negative offset